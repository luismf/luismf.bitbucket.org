var targetLocation = "#chart-simulation";
var canvasw = 500;
var canvash = 250;
var chartw  = canvasw;
var charth  = canvash;
var xPadding = 15;
var yPadding = 25;
var samplingRate = 500;
var lengthTrial  = 3;
var offsetTrial  = 1.5;

var x = d3.range(samplingRate * lengthTrial).map(function(d){return d/(samplingRate) - offsetTrial});
var y = x.map(function(d){return jStat.normal.sample(0,1)});

var data = x.map(function(_, idx){return{x:x[idx], y:y[idx]}});
      //Mumbo jumbo for creating a dictionary more friendly to d3

var xScale = d3.scaleLinear()
                .domain([d3.min(x), d3.max(x)])
                .range([0+xPadding, chartw-xPadding]);

var yScale = d3.scaleLinear()
                .domain([d3.min(y), d3.max(y)])
                .range([charth-yPadding, 0+yPadding]);

var svg = d3.select(targetLocation) //Create svg canvas
              .append("svg")
              .attr("width", canvasw)
              .attr("height", canvash);

var lineFunction = d3.line()
                      .x(function(d) {return xScale(d.x)})
                      .y(function(d) {return yScale(d.y)});

var lineGraph = svg.append("path")
                    .datum(data)
                    .attr("d", lineFunction)
                    .attr("stroke", "steelblue")
                    .attr("stroke-width", 2)
                    .attr("fill", "none");

var smoothY = movMean(y, 1).map(function (d){return d});

var smoothData = x.map(function(_, idx){return{x:x[idx], y:smoothY[idx]}});

svg.on("click", function(){lineGraph.transition().attr("d", lineFunction(smoothData))duration(1000);});
