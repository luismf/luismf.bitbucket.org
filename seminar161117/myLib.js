/**
 * Calculates the moving average of an dasharray, it pads with zero beginning and end
 *@ param {number} array - Numerical array
 *@ param {number} halfWindowLength - Size of the window will be (halfWindowLength*2)+1


 */
function movMean(values, halfWindowLength){

var padder = d3.range(halfWindowLength).map(function(d){return 0});
var paddedValues = padder.concat(values).concat(padder);


  for (idx=halfWindowLength; idx < paddedValues.length-halfWindowLength-1;  idx++) {

    values[idx - halfWindowLength] = d3.mean(paddedValues.slice(idx-halfWindowLength, idx+halfWindowLength+1));

  }

  return values
};
