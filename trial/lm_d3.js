lm_d3 = {
  exAppParagraph: function () {
    var myArr = [
      1,
      3,
      4,
      5
    ];
    d3.select('body').append('p').text('hola');
  },
  exAppParagraphData: function () {
    var myArr = [
      'a',
      'b',
      'c',
      'd'
    ];
    d3.select('body').append('div').selectAll('p').data(myArr).enter().append('p').text(function (d, i) {
      return d
    }).style('color', function (d) {
      if (d == 'a') {
        return 'red';
      } else {
        return 'black';
      }
    });
  },
};
lm_d3.exAppParagraphData();
