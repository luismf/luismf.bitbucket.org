var ndist = jStat.normal(0,1) // We chose a normal mu = 0, sigma = 1

function toDensity(x){
    return (x/0.1)/sampNumber;
}


// Canvas size, using margin convention d3
var margin = {top: 50, right: 30, bottom: 50, left: 60};
var width = 900 - margin.left - margin.right;
var height = 500 - margin.top - margin.bottom;

var svg = d3.select("#container").append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")")

// Generate data
var ll = -4
    ul = 4
    step = 0.1

var data = d3.range(ll,ul+step,step).map(function (d){return {"x": d, "y": ndist.pdf(d)}})
var data2 = d3.range(ll,ul+step,step).map(function (d){return {"x": d, "y": 0}})

// Create scales
var xScale = d3.scaleLinear()
    .domain([d3.min(data, function(d){ return d.x; }), d3.max(data, function(d){ return d.x; })])
    .range([0, width])
    .nice()

var yScaleDist = d3.scaleLinear()
    .domain([0, d3.max(data, function(d){ return d.y; })])
    .range([height, 0])
    .nice()

var yScaleHist = d3.scaleLinear()
    .domain([0, d3.max(data, function(d){ return d.y; })])
    .rangeRound([height, 0])
    .nice()
    .clamp(true);

var xScaleHist = d3.scaleLinear()
    .domain([d3.min(data, function(d){ return d.x; }), d3.max(data, function(d){ return d.x; })])
    .rangeRound([0, width])
    .nice()

// Some axis
var xAxis = d3.axisBottom()
    .scale(xScale);
svg.append("g")
    .classed("xdist", true)
    .call(xAxis)
    .attr("transform", "translate(0," + (height) + ")")

var yAxis = d3.axisLeft()
    .scale(yScaleDist);
svg.append("g")
    .classed("ydist", true)
    .call(yAxis)

// var yAxisHist = d3.axisRight()
//     .scale(yScaleHist)
// svg.append("g")
//     .call(yAxisHist)
//     .classed("yhist", true)
//     .attr("transform", "translate(" + (width) +",0)")


//Title
svg.append("text")
    .attr("y", 0-margin.top)
    .attr("x", xScale(0))
    .attr("dy", "1em")
    .classed("title", true)
    .style("text-anchor", "middle")
    .text("Normal distribution");


//Axes labels
svg.append("text")
    .attr("transform", "rotate(-90)")
    .attr("y", 0 - margin.left)
    .attr("x",0 - (height / 2))
    .attr("dy", "1em")
    .style("text-anchor", "middle")
    .text("Probability density");

svg.append("text")
    .attr("y", yScaleDist(0)+20)
    .attr("x", xScale(0))
    .attr("dy", "1em")
    .style("text-anchor", "middle")
    .text("Value");

//Sample counter
var counterText = svg.append("text")
    .attr("y", 0)
    .attr("x", width-100)
    .attr("dy", "1em")
    .style("text-anchor", "middle")
    .text(0 + " Samples");


var line = d3.line()
    .x(function(d){ return xScale(d.x); })
    .y(function(d){ return yScaleDist(d.y); })

var sampNumber = 1;

//Drawing the histogram with random realizations from the normal distribution
var sampData = []
var histogram = d3.histogram()
    .value(function(d) { return d;})
    .domain(xScale.domain())
    .thresholds(d3.range(ll,ul+0.1,0.1));

var bins = histogram(sampData)
// yScaleHist.domain([0, d3.max(bins, function(d) {return toDensity(d.length); })]);
// svg.select(".yhist").transition().call(yAxisHist)

var histChart = svg.selectAll("rect")
    .data(bins)
    .enter().append("rect")
    .attr("class", "bar")
    .attr("x", 1)
    .attr("transform", function(d) {
        return "translate(" + xScale(d.x0) + "," + yScaleHist(d.length) + ")"; })
    .attr("width", function(d) { return xScale(d.x1) - xScale(d.x0) -1 ; })
    .attr("height", function(d) { return height - yScaleHist(d.length); });

var path = svg.append("path")
    .datum(data)
    .attr("class", "line")
    .attr("d", line);

var outer = 0;

function addN(N){
    var inner;
    sampNumber += N;
    console.log("Eo")
    for (inner = 0; inner < N; inner++) {
        sampData.push(ndist.sample())
    }
}

function animate(N){
    if (sampData.length>1000*bins.length){
        // t.stop();
    }
    else {
        outer++
    }
        addN(N)
        bins = histogram(sampData)
        // yScaleHist.domain([0, d3.max(bins, function(d) {return toDensity(d.length)})]);
        // svg.select(".yhist").transition().call(yAxisHist);
        histChart.data(bins)
            .transition()
            .attr("transform", function (d) {
                return "translate(" + xScaleHist(d.x0) + "," + yScaleHist(toDensity(d.length)) + ")";
            })
            .attr("height", function (d) {
                return height - yScaleHist(toDensity(d.length));
            });
        counterText.text(d3.format(".3s")(sampNumber) + " Samples")
}

svg.on("click", animate)

function animate1() {animate(1)};
function animate10() {animate(10)};
function animate100() {animate(100)};
function animate1000() {animate(1000)};
function animate10000() {animate(10000)};


// t = d3.interval(animate, 1000);



// Some silly animations not for using
// function end1(){
// path.datum(data2).transition().duration(1000).attr("d", line).on("end", end2)}
//
// function end2(){
//     path.datum(data).transition().duration(1000).attr("d", line).on("end",end1)
// }
//
// end1()

